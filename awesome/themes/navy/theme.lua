-- Ref
-- https://github.com/lcpz/awesome-copycats



local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi
local sharedtags = require("sharedtags")
local utils = require("utils")

local os = os
local my_table = awful.util.table or gears.table


-- local wp_selected = scandir(wp_path)
-- num_files = 10
--randint = 10

--local wp_temp = gears.filesystem.get_random_file_from_dir('/home/alex/pictures/wallpapers/')


local theme         = {}

theme.wp_path       = os.getenv("HOME") .. "/pictures/wallpapers/"
theme.wp_default    = os.getenv("HOME") .. "/pictures/wallpapers/laurynas-mereckas-1506337-unsplash.jpg"
theme.wallpaper     = theme.wp_path .. utils.filesystem.get_random_file_from_dir(theme.wp_path)

-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
--
theme.font          = "Fira Mono 10"
-- theme.fg_normal     = "#6F8AB7"
theme.fg_normal     = "#5050a5"
theme.fg_focus      = "#9f9fd1"
theme.fg_urgent     = "#e5446d"
theme.bg_normal     = "#141529"
theme.bg_focus      = "#1f2041"
theme.bg_urgent     = "#7C403C"
theme.border_width  = dpi(1)
--theme.border_normal =
--theme.border_focus  =
--theme.border_marked =
theme.useless_gap   = dpi(2)

local markup = lain.util.markup
local gray = "#94928F"

local mytextclock = wibox.widget.textclock(" %H:%M ")
mytextclock.font = theme.font

-- Calendar
theme.cal = lain.widget.cal({
    attach_to = { mytextclock },
    notification_preset = {
        font = "Fira Mono 8",
        fg = theme.fg_normal,
        bg = theme.bg_normal
    }

})

local cpu = lain.widget.sysload({
    settings = function()
        widget:set_markup(markup.font(theme.font, markup(gray, " cpu ") .. load_1 .. " "))
    end
})
-- MEM
local mem = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.font(theme.font, markup(gray, " mem ") .. string.format("%.2f", (100 * mem_now.used/ mem_now.total)) .. "% "))
    end
})
-- Battery
local bat = lain.widget.bat({
    settings = function()
        local perc = bat_now.perc
        if bat_now.ac_status == 1 then perc = perc .. " Plug" end
        widget:set_markup(markup.font(theme.font, markup(gray, " bat ") .. perc .. " "))
    end
})

-- Net checker
local net = lain.widget.net({
    settings = function()
        if net_now.state == "up" then net_state = "On"
        else net_state = "Off" end
        widget:set_markup(markup.font(theme.font, markup(gray, " net ") .. net_state .. " "))
    end
})
--
-- ALSA volume
theme.volume = lain.widget.alsa({
    settings = function()
        header = " vol "
        vlevel  = volume_now.level

        if volume_now.status == "off" then
            vlevel = vlevel .. "M "
        else
            vlevel = vlevel .. " "
        end

        widget:set_markup(markup.font(theme.font, markup(gray, header) .. vlevel))
    end
})

--separators
local first = wibox.widget.textbox(markup.font("Fira Mono 5", " "))
local spr   = wibox.widget.textbox('     ')


-- local function set_wallpaper(s)
--     awful.util.spawn('bash -c "feh --randomize --bg-fill %s"', theme.wallpapers)
-- end


theme.tags = sharedtags({
    { layout = awful.layout.layouts[1]},
    { screen = 2, layout = awful.layout.layouts[1]},
    { layout = awful.layout.layouts[1]},
    { screen = 2, layout = awful.layout.layouts[1]},
    { layout = awful.layout.layouts[1]},
    { screen = 2, layout = awful.layout.layouts[1]},
    { layout = awful.layout.layouts[1]},
    { screen = 2, layout = awful.layout.layouts[1]},
    { layout = awful.layout.layouts[1]},
})

function theme.at_screen_connect(s)
    -- s.quake = lain.util.quake({ app = awful.util.terminal })

    -- Wallpaper
    local wallpaper = theme.wallpaper
    --set_wallpaper(s)
    gears.wallpaper.maximized(wallpaper, s, false)

    -- Each screen has its own tag table.
    -- awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])
    sharedtags.viewonly(theme.tags[1],s)


    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }


    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            first,
            spr,
            -- mylauncher,
            s.mytaglist,
            spr,
            s.mypromptbox,
            spr,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            -- mykeyboardlayout,
            spr,
            wibox.widget.systray(),
            spr,
            cpu.widget,
            mem.widget,
            bat.widget,
            net.widget,
            theme.volume.widget,
            mytextclock,
            s.mylayoutbox,
            spr,
        },
    }
end
-- }}}
return theme
