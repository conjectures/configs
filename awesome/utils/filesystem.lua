

local Gio = require("lgi").Gio
local gstring = require("gears.string")
local gtable = require("gears.table")
local udebug = require("utils.debug")

math.randomseed(os.time())

local filesystem = {}

function filesystem.get_random_file_from_dir(path, exts, absolute_path)
    local files, valid_exts = {}, {}

    -- Transforms { "jpg", ... } into { [jpg] = #, ... }
    if exts then for i, j in ipairs(exts) do valid_exts[j:lower():gsub("^[.]", "")] = i end end

    -- Build a table of files from the path with the required extensions
    local file_list = Gio.File.new_for_path(path):enumerate_children("standard::*", 0)

    -- This will happen when the directory doesn't exist.
    if not file_list then return nil end

    for file in function() return file_list:next_file() end do
        if file:get_file_type() == "REGULAR" then
            local file_name = file:get_display_name()
            -- udebug.dump('\n' .. file_name  )

            if not exts or valid_exts[file_name:lower():match(".+%.(.*)$") or ""] then
               table.insert(files, file_name)
            end
        end
    end

    if #files == 0 then return nil end

    -- udebug.dump('\n number of files: ' .. #files)
    -- Return a randomly selected filename from the file table
    local file = files[math.random(#files)]

    -- udebug.dump('\n selected file: ' .. file)


    return absolute_path and (path:gsub("[/]*$", "") .. "/" .. file) or file
end

return filesystem
